﻿using System;
using UnityEngine;

namespace SaltboxGames.Utils
{
    public static class ArrayExtensions
    {
        public static T GetRandom<T>(this T[] val)
        {
            if (val.Length == 1)
            {
                return val[0];
            }
            return val[UnityEngine.Random.Range(0, val.Length)];
        }

        public static T[] Shuffle<T>(this T[] val)
        {
            if (val.Length == 1)
            {
                return val;
            }

            int n = val.Length;
            while (n > 1)
            {
                n--;
                int k = UnityEngine.Random.Range(0, n + 1);
                T temp = val[n];
                val[n] = val[k];
                val[k] = temp;
            }
            return val;
        }

        public static T[] Shuffled<T>(this T[] val)
        {
            T[] arr = new T[val.Length];
            Array.Copy(val, arr, arr.Length);
            arr.Shuffle();
            return arr;
        }
    }
}