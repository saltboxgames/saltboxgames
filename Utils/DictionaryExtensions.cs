﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaltboxGames.Utils
{
    public static class DictionaryExtensions
    {
        public static void AddElement<T1, T2>(this Dictionary<T1, List<T2>> dictionary, T1 key, T2 value)
        {
            List<T2> list;
            if(dictionary.TryGetValue(key, out list))
            {
                list.Add(value);
            }
            else
            {
                list = new List<T2>();
                list.Add(value);
                dictionary.Add(key, list);
            }
        }

        public static void RemoveElement<T1, T2>(this Dictionary<T1, List<T2>> dictionary, T1 key, T2 value)
        {
            List<T2> list;
            if (dictionary.TryGetValue(key, out list))
            {
                list.Remove(value);
                if(list.Count == 0)
                {
                    dictionary.Remove(key);
                }
            }
        }
    }
}
