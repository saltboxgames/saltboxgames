﻿using System;
using System.IO;
using UnityEngine;

namespace SaltboxGames.Storage
{
    public class JsonStorage : DataStorage
    {
        public bool load<T>(string name, out T obj)
        {
            
            using (StreamReader reader = new StreamReader(Path.Combine(Application.persistentDataPath, name, ".json")))
            {
                string data = reader.ReadToEnd();
                obj = JsonUtility.FromJson<T>(data);
                if(obj != null)
                {
                    return true;
                }
            }
            obj = default(T);
            return false;
        }

        public void save<T>(string name, T obj)
        {
            using (StreamWriter writer = new StreamWriter(Path.Combine(Application.persistentDataPath, name, ".json"), false))
            {
                string data = JsonUtility.ToJson(obj);
                writer.Write(data);
            }
        }
    }
}
