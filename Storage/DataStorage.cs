﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaltboxGames.Storage
{
    public interface DataStorage
    {
        bool load<T>(string name, out T obj);
        void save<T>(string name, T obj);
    }
}
