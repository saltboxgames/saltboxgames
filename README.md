# Saltbox Games

Simple Library of thigs we find we use a lot.
_In Unity_

### Installing

The way we typically use this library is to add it as submodule under the assets folder in your project.
All objects are under the `SaltboxGames` namespace.

```
    git submodule add git@bitbucket.org:saltboxgames/saltboxgames.git Assets/SaltboxGames
```

### Contributing

If you've got improvements you'd like to add,
Or have you're own Tools you'd think would fit.
Feel free to make a pull-request! and we'll check it out eventually!
