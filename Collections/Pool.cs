﻿using System;
using System.Collections.Generic;

namespace SaltboxGames.Collections
{
    public class Pool<T>
    {
        private Stack<T> allObjects;
        private Func<T> alloc;

        public Pool(Func<T> alloc)
        {
            allObjects = new Stack<T>();
            this.alloc = alloc;
        }

        public T Take()
        {
            if (allObjects.Count > 0)
            {
                return allObjects.Pop();
            }
            return alloc();
        }

        public void Add(T obj)
        {
            allObjects.Push(obj);
        }
    }
}
